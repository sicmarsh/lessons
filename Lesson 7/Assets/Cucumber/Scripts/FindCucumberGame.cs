﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class FindCucumberGame : MonoBehaviour
{
    [Header("Settings")]
    [Space(3)]
    [SerializeField] private int m_numOfCucumbers = 3;
    [SerializeField] private int m_numOfObjects = 10;

    [Header("Settings")]
    [Space(3)]
    [SerializeField] private GameObject m_basePrefab;

    private bool m_isVictory;
    private int m_currNumOfObjects;
    private float m_reloadLeveltimer;
    private GameObject[] m_allObjects;

    private void Start()
    {
        m_currNumOfObjects = m_numOfObjects;
        BuildCucumberScene();
    }

    private void Update()
    {
        if (!m_isVictory)
        {
            //Play
            DestroyObjectOnClick();

            if (IsEndOfGame())
            {                
                FallObjects();

                m_reloadLeveltimer = Time.time + 1.5f;
            }
        }
        else
        {
            //Set next level

            if(Time.time > m_reloadLeveltimer)
            {
                m_currNumOfObjects++;
                ResetVictroyState();
                DestroyAllObjects();
                BuildCucumberScene();
            }   
            
        }
    }

    private void ResetVictroyState()
    {
        m_isVictory = false;
    }

    private void DestroyAllObjects()
    {
        for (int i = 0; i < m_allObjects.Length; i++)
        {
            if (m_allObjects[i] != null)
            {
                Destroy(m_allObjects[i]);
            }
        }
    }

    private bool IsEndOfGame()
    {
        int deletedObj = 0;
        int numOfCucumbers = 0;

        for (int i = 0; i < m_allObjects.Length; i++)
        {
            GameObject currObj = m_allObjects[i];

            if (currObj == null)
            {
                deletedObj++;
            }
            else if (currObj.GetComponent<MeshRenderer>().material.color == Color.green)
            {
                numOfCucumbers++;
            }
        }

        if (numOfCucumbers == 0 || deletedObj > 2)
        {
            m_isVictory = CheckVictoryState(numOfCucumbers);
            return true;
        }
        return false;
    }

    private bool CheckVictoryState(int numOfCucumbers)
    {
        if (numOfCucumbers > 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            return false;
        }
        else
        {
            return true;
        }
    }

    private void BuildCucumberScene()
    {
        m_allObjects = new GameObject[m_currNumOfObjects];
        Color[] colors = new Color[] { Color.red, Color.blue, Color.yellow, Color.black, Color.gray };
        int cucumbers = m_numOfCucumbers;
        int otherObjects = m_currNumOfObjects - m_numOfCucumbers;
        int objIndex = 0;

        while (cucumbers > 0)
        {
            if (TryCreateOnject(Color.green, objIndex))
            {
                cucumbers--;
                objIndex++;
            }
        }

        while (otherObjects > 0)
        {
            int rndIndex = Random.Range(0, colors.Length);

            if (TryCreateOnject(colors[rndIndex], objIndex))
            {
                otherObjects--;
                objIndex++;
            }
        }
    }

    private bool TryCreateOnject(Color clr, int objIndex)
    {
        float posX = Random.Range(-4, 4.5f) + Random.Range(-0.15f, 0.15f);
        float posY = Random.Range(-4, 4.5f) + Random.Range(-0.15f, 0.15f);

        Vector3 pos = new Vector3(posX, posY, 0);
        Quaternion rot = Random.rotation;

        for (int i = 0; i < m_allObjects.Length; i++)
        {
            GameObject currObj = m_allObjects[i];
            if (currObj != null && Vector3.Distance(pos, currObj.transform.position) < 1.1f)
            {
                return false;
            }
        }

        GameObject instance = Instantiate(m_basePrefab, pos, rot);
        instance.GetComponent<MeshRenderer>().material.color = clr;
        m_allObjects[objIndex] = instance;
        return true;
    }

    private static void DestroyObjectOnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 50f))
            {
                Destroy(hit.collider.gameObject);
            }
        }
    }

    private void FallObjects()
    {
        for(int i = 0; i < m_allObjects.Length; i++)
        {
            if(m_allObjects[i] != null)
            {
                m_allObjects[i].GetComponent<Rigidbody>().isKinematic = false;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSelector : MonoBehaviour
{
    [SerializeField] private GameObject[] m_allBlocks;
    [SerializeField] private Transform m_selectorPoint;
    private GameObject m_currBlock = null;
    private int m_currIndex = 0;
    private int m_prevIndex = 0;

    public GameObject SelectedBlock { get { return m_currBlock; } }

    private void Start()
    {
        CreateCurrentCube();
    }

    private void CreateCurrentCube()    
    {
        Vector3 pos = m_selectorPoint.position;
        Quaternion rot = m_selectorPoint.rotation;
        m_currBlock = Instantiate(m_allBlocks[m_currIndex], pos, rot, transform);
    }

    private void Update()
    {
        float scrl = Input.mouseScrollDelta.y;

        if (scrl != 0)
        {
            if (scrl > 0)
            {
                m_currIndex++;
            }
            else
            {
                m_currIndex--;
            }

            m_currIndex = (int)Mathf.Repeat(m_currIndex, m_allBlocks.Length - 1);
        }
        if (m_currIndex != m_prevIndex)
        {
            if (m_currBlock != null)
            {
                Destroy(m_currBlock);
            }

            CreateCurrentCube();

            m_prevIndex = m_currIndex;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    [SerializeField] private float m_rotationSpeed;
    [SerializeField] private float m_movementSpeed;

    private void Update()
    {
        float dt = Time.deltaTime;
        DoRotation(dt);
        DoMovement(dt);
    }

    private void DoMovement(float dt)
    {
        float mSpeed = m_movementSpeed * dt;

        float hSpeed = Input.GetAxis("Horizontal") * mSpeed;
        float fSpeed = Input.GetAxis("Straight") * mSpeed;
        float vSpeed = Input.GetAxis("Vertical") * mSpeed;

        transform.Translate(transform.forward * fSpeed, Space.World);
        transform.Translate(transform.right * hSpeed, Space.World);
        transform.Translate(transform.up * vSpeed, Space.World);
    }

    private void DoRotation(float dt)
    {
        float xRotSpeed = Input.GetAxis("Mouse X") * m_rotationSpeed * dt;
        float yRotSpeed = Input.GetAxis("Mouse Y") * m_rotationSpeed * dt;

        transform.RotateAround(transform.position, Vector3.up, xRotSpeed);
        transform.RotateAround(transform.position, transform.right, -yRotSpeed);
    }
}

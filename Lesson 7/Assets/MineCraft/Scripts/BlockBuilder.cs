﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockBuilder : MonoBehaviour
{
    private enum Action
    {
        None = -1,
        Build = 0,
        Destroy = 1
    }

    private BlockSelector m_selector;
    private GameObject m_mainCube;
    public Text aim;

    // Use this for initialization
    void Start()
    {
        m_selector = GetComponent<BlockSelector>();
        m_mainCube = GameObject.Find("MAINCUBE");
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Action action = Action.None;

        if (Input.GetMouseButtonUp(0))
        {
            action = Action.Build;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            action = Action.Destroy;
        }

        if (action == Action.None) return;

        Ray ray = Camera.main.ScreenPointToRay(aim.transform.position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 30f))
        {
            GameObject targetBlock = hit.collider.gameObject;
            if (action == Action.Build)
            {
                // build
                Vector3 pos = FindBlockPos(targetBlock.transform.position, hit.point);
                CreateBlock(m_selector.SelectedBlock, pos, m_mainCube.transform);
            }
            else
            {
                // destroy
                Destroy(targetBlock);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void CreateBlock(GameObject prefab, Vector3 pos, Transform parent)
    {
        if (Vector3.Distance(transform.position, pos) > 1.5f)
        {
            //build
            GameObject instance = Instantiate(prefab, pos, Quaternion.identity, parent);
        }
        else
        {
            Debug.Log("to close!");
        }
    }

    private Vector3 FindBlockPos(Vector3 boxPoint, Vector3 hitPoint)
    {
        float x = CalcTranslationPoint(boxPoint.x, hitPoint.x);
        float y = CalcTranslationPoint(boxPoint.y, hitPoint.y);
        float z = CalcTranslationPoint(boxPoint.z, hitPoint.z);
        Vector3 translation = new Vector3(x, y, z);
        Vector3 result = boxPoint + translation;
        return result;
    }

    private int CalcTranslationPoint(float p1, float p2)
    {
        int result = 0;

        if (Mathf.Abs(p1 - p2) > 0.45f)
        {
            if (p1 > p2)
            {
                result = -1;
            }
            else if (p1 < p2)
            {
                result = 1;
            }
        }

        return result;
    }
}
